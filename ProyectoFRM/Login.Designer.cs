﻿namespace ProyectoFRM
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbAero = new System.Windows.Forms.ComboBox();
            this.cb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cbAero
            // 
            this.cbAero.FormattingEnabled = true;
            this.cbAero.Location = new System.Drawing.Point(170, 67);
            this.cbAero.Name = "cbAero";
            this.cbAero.Size = new System.Drawing.Size(121, 21);
            this.cbAero.TabIndex = 0;
            this.cbAero.SelectedIndexChanged += new System.EventHandler(this.cbAero_SelectedIndexChanged);
            // 
            // cb
            // 
            this.cb.FormattingEnabled = true;
            this.cb.Location = new System.Drawing.Point(418, 67);
            this.cb.Name = "cb";
            this.cb.Size = new System.Drawing.Size(121, 21);
            this.cb.TabIndex = 1;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cb);
            this.Controls.Add(this.cbAero);
            this.Name = "Login";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbAero;
        private System.Windows.Forms.ComboBox cb;
    }
}