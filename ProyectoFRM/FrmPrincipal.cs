﻿using ProyectBOL;
using ProyectENT;
using ProyectoFRM.WSAeropuertos;
using ProyectoFRM.WSAerolineas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoFRM.WSVuelo;

namespace ProyectoFRM
{
    public partial class FrmPrincipal : Form
    {
        private AeropuertoBOL ubo;
        public AeropuertoENT Aeropuerto { get; set; }

        private AerolineaBOL abo;
        public AerolineaENT Aerolinea { get; set; }

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmAeropuertos_Load(object sender, EventArgs e)
        {

            ubo = new AeropuertoBOL();
            abo = new AerolineaBOL();
            


        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

            

        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {


            ubo.Delete();

            int dia = dateTimePicker1.Value.Day;
            int mes = dateTimePicker1.Value.Month;
            int año = dateTimePicker1.Value.Year;

            airportsServiceClient ws = new airportsServiceClient("airportsServicePort");
            airport[] aeropuertos = ws.airports("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d", año, mes, dia, "");




            try
            {
                AeropuertoENT aeropuerto = new AeropuertoENT
                {
                    Id = Aeropuerto == null ? 0 : Aeropuerto.Id
                };

                var listaAeropuertos = new List<AeropuertoENT>();

                   

                foreach (airport item in aeropuertos)
                {

                    aeropuerto = new AeropuertoENT();

                    aeropuerto.Nombre = item.name;
                    aeropuerto.Pais = item.countryName;
                    aeropuerto.Ciudad = item.city;
                    aeropuerto.Activo = true;

                    listaAeropuertos.Add(aeropuerto);



                }
                ubo.Guardar(listaAeropuertos);
                MessageBox.Show("Lista Aeropuertos actualizada exitosamente!!");

           
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }

        private void btnAerolineas_Click(object sender, EventArgs e)
        {
            abo.Delete();


            int dia = dateTimePicker1.Value.Day;
            int mes = dateTimePicker1.Value.Month;
            int año = dateTimePicker1.Value.Year;

            airlinesServiceClient wsa = new airlinesServiceClient("airlinesServicePort" );
            
            airline[] aerolineas = wsa.airlines("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d", año, mes, dia, "");
             

            try
            {
                AerolineaENT aerolinea = new AerolineaENT
                {
                    Id = Aerolinea == null ? 0 : Aerolinea.Id
                };

                var listaAerolineas = new List<AerolineaENT>();



                foreach (airline item in aerolineas)
                {

                    aerolinea = new AerolineaENT();

                    aerolinea.Nombre = item.name;
                    
                    aerolinea.Activo = item.active;

                    listaAerolineas.Add(aerolinea);



                }
                abo.Guardar(listaAerolineas);
                MessageBox.Show("Lista Aerolineas actualizada exitosamente!!");




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }

        private void btnVuelos_Click(object sender, EventArgs e)
        {
            flightServiceClient wsv = new flightServiceClient("flightServicePort");

            responseFlightStatus   estado  = wsv.flightStatus_arr("ed9a785b", "d8870be5ac62b2d6ddcfb041ea4aaf3d", "AA", "100", 2018, 08, 25, "", "", "", "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmAerolineas frmAero = new FrmAerolineas();
            Hide();
            frmAero.ShowDialog();
            Show();
        }

        private void btnManAerpuertos_Click(object sender, EventArgs e)
        {
            FrmAeropuertos frmAero = new FrmAeropuertos();
            Hide();
            frmAero.ShowDialog();
            Show();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    }

