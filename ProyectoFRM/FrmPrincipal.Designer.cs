﻿namespace ProyectoFRM
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnAerolineas = new System.Windows.Forms.Button();
            this.btnVuelos = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnManAerolineas = new System.Windows.Forms.Button();
            this.btnManAerpuertos = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Selecione Día:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(164, 18);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 5;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(28, 65);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(135, 23);
            this.btnEnviar.TabIndex = 6;
            this.btnEnviar.Text = "Actualizar Aeropuertos";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnAerolineas
            // 
            this.btnAerolineas.Location = new System.Drawing.Point(229, 65);
            this.btnAerolineas.Name = "btnAerolineas";
            this.btnAerolineas.Size = new System.Drawing.Size(135, 23);
            this.btnAerolineas.TabIndex = 13;
            this.btnAerolineas.Text = "Actualizar Aerolineas";
            this.btnAerolineas.UseVisualStyleBackColor = true;
            this.btnAerolineas.Click += new System.EventHandler(this.btnAerolineas_Click);
            // 
            // btnVuelos
            // 
            this.btnVuelos.Location = new System.Drawing.Point(10, 33);
            this.btnVuelos.Name = "btnVuelos";
            this.btnVuelos.Size = new System.Drawing.Size(118, 23);
            this.btnVuelos.TabIndex = 16;
            this.btnVuelos.Text = "Consultar Vuelos";
            this.btnVuelos.UseVisualStyleBackColor = true;
            this.btnVuelos.Click += new System.EventHandler(this.btnVuelos_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.btnAerolineas);
            this.groupBox1.Controls.Add(this.btnEnviar);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(390, 123);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actualizar";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnManAerpuertos);
            this.groupBox2.Controls.Add(this.btnManAerolineas);
            this.groupBox2.Location = new System.Drawing.Point(495, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(253, 123);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mantenimientos";
            // 
            // btnManAerolineas
            // 
            this.btnManAerolineas.Location = new System.Drawing.Point(30, 19);
            this.btnManAerolineas.Name = "btnManAerolineas";
            this.btnManAerolineas.Size = new System.Drawing.Size(195, 23);
            this.btnManAerolineas.TabIndex = 0;
            this.btnManAerolineas.Text = "Mantenimiento Aerolineas";
            this.btnManAerolineas.UseVisualStyleBackColor = true;
            this.btnManAerolineas.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnManAerpuertos
            // 
            this.btnManAerpuertos.Location = new System.Drawing.Point(30, 65);
            this.btnManAerpuertos.Name = "btnManAerpuertos";
            this.btnManAerpuertos.Size = new System.Drawing.Size(195, 23);
            this.btnManAerpuertos.TabIndex = 1;
            this.btnManAerpuertos.Text = "Mantenimiento Aeropuertos";
            this.btnManAerpuertos.UseVisualStyleBackColor = true;
            this.btnManAerpuertos.Click += new System.EventHandler(this.btnManAerpuertos_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnVuelos);
            this.groupBox3.Location = new System.Drawing.Point(12, 176);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(751, 212);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Consultas";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = global::ProyectoFRM.Properties.Resources.salir;
            this.btnSalir.Location = new System.Drawing.Point(688, 394);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 55);
            this.btnSalir.TabIndex = 20;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPrincipal";
            this.Load += new System.EventHandler(this.FrmAeropuertos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Button btnAerolineas;
        private System.Windows.Forms.Button btnVuelos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnManAerpuertos;
        private System.Windows.Forms.Button btnManAerolineas;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSalir;
    }
}