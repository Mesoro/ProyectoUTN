﻿using ProyectBOL;
using ProyectENT;
using ProyectoFRM.WSAeropuertos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFRM
{
    public partial class FrmAeropuertos : Form
    {
        private AeropuertoBOL ubo;
        

        public FrmAeropuertos()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            SetFontAndColors();
            ubo = new AeropuertoBOL();

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                FrmPrincipal frmUsuario = new FrmPrincipal
                {
                    
                    Aeropuerto = (AeropuertoENT)dgvUsuarios.SelectedRows[0].DataBoundItem

                };
                Hide();
                frmUsuario.ShowDialog();

                Show();
            }
            else
            {
                MessageBox.Show("Favor seleccionar un aeropuerto.");
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                AeropuertoENT Aeropuerto = (AeropuertoENT)dgvUsuarios.SelectedRows[0].DataBoundItem;
                string texto = String.Format("Está seguro  que desea eliminar a {0}", Aeropuerto.Nombre);
                if (MessageBox.Show(texto, "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ubo.Eliminar(Aeropuerto);
                    Refrescar();
                }
            }
        }
        private void Refrescar()
        {
            dgvUsuarios.DataSource = ubo.CargarAeropuertos(txtFiltro.Text.Trim());

        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            if (txtFiltro.Text.Trim().Length >= 3)
            {
                Refrescar();
            }
            else
            {
                dgvUsuarios.DataSource = null;
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void aeropuertoENTBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void SetFontAndColors()
        {
            dgvUsuarios.DefaultCellStyle.Font = new Font("Tahoma", 15);
            dgvUsuarios.DefaultCellStyle.ForeColor = Color.Blue;
            dgvUsuarios.DefaultCellStyle.BackColor = Color.Beige;
            dgvUsuarios.DefaultCellStyle.SelectionForeColor = Color.Yellow;
            dgvUsuarios.DefaultCellStyle.SelectionBackColor = Color.Black;
            
        }
    }
}
