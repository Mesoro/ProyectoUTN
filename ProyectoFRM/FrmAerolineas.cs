﻿using ProyectBOL;
using ProyectENT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFRM
{
    public partial class FrmAerolineas : Form
    {
        private AerolineaBOL ubo;

        public FrmAerolineas()
        {
            InitializeComponent();
        }

        private void FrmAerolineas_Load(object sender, EventArgs e)
        {
            SetFontAndColors();
            ubo = new AerolineaBOL();
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Close(); 
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                AerolineaENT Aerolinea = (AerolineaENT)dgvUsuarios.SelectedRows[0].DataBoundItem;
                string texto = String.Format("Está seguro  que desea eliminar a {0}", Aerolinea.Nombre);
                if (MessageBox.Show(texto, "Eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ubo.Eliminar(Aerolinea);
                    Refrescar();
                }
            }
        }
        private void Refrescar()
        {
            dgvUsuarios.DataSource = ubo.CargarAerolineas(txtFiltro.Text.Trim());
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            if (txtFiltro.Text.Trim().Length >= 3)
            {
                Refrescar();
            }
            else
            {
                dgvUsuarios.DataSource = null;
            }
        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void SetFontAndColors()
        {
            dgvUsuarios.DefaultCellStyle.Font = new Font("Tahoma", 15);
            dgvUsuarios.DefaultCellStyle.ForeColor = Color.Blue;
            dgvUsuarios.DefaultCellStyle.BackColor = Color.Beige;
            dgvUsuarios.DefaultCellStyle.SelectionForeColor = Color.Yellow;
            dgvUsuarios.DefaultCellStyle.SelectionBackColor = Color.Black;

        }
    }
}
