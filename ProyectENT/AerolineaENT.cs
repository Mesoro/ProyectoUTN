﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectENT
{
    public class AerolineaENT
    {
        public int Id { get; set; }
        public string Nombre  { get; set; }
        public bool Activo { get; set; }
    }
}
