﻿using Npgsql;
using ProyectENT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectDAL
{
    public class AerolineaDAL
    {

        public AerolineaENT CargarAerolineas(NpgsqlDataReader reader)
        {

            AerolineaENT aero = new AerolineaENT
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                
                Activo = reader.GetBoolean(2),



            };
            return aero;
        }

        public AerolineaENT CargarAerolineasId(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {


                con.Open();
                string sql = "select id, nombre, activo   from aerolinea where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    AerolineaENT aero = new AerolineaENT
                    {


                        Id = reader.GetInt32(0),
                        Nombre = reader.GetString(1),
                        
                        Activo = reader.GetBoolean(2),

                    };
                    return aero;
                }
            }
            return new AerolineaENT();
        }

        public void Insertar(IEnumerable<AerolineaENT> listaAerolineas)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                foreach (var item in listaAerolineas)
                {

                    string sql = "INSERT INTO public.aerolinea(nombre )" +
                        " VALUES(@nom)";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@nom", item.Nombre);
                    

                    cmd.ExecuteNonQuery();

                }


            }

        }
            public void Eliminar(AerolineaENT a)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE aerolinea " +
                    " SET activo = false " +
                    " WHERE id =@id ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", a.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public List<AerolineaENT> CargarAerolineas(string filtro)
        {
            List<AerolineaENT> aerolineas = new List<AerolineaENT>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select id,  nombre, activo from aerolinea " +
                    " order by nombre asc";

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = " SELECT id, nombre, activo " +
                        "FROM public.aerolinea " +
                        "WHERE lower(nombre) like lower(@par) ";
                        
                }

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@par", filtro + "%");
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {


                    aerolineas.Add(CargarAerolineas(reader));
                }

            }
            return aerolineas;
        }

        public void Delete()
        {
            try
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    string sql = "DELETE from aerolinea ";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
