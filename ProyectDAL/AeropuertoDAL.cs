﻿using Npgsql;
using ProyectENT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectDAL
{
    public class AeropuertoDAL
    {
        public AeropuertoENT CargarAeropuertos(NpgsqlDataReader reader)
        {

            AeropuertoENT aero = new AeropuertoENT
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Ciudad = reader.GetString(2),
                Pais = reader.GetString(3),
                Activo = reader.GetBoolean(4),



            };
            return aero;
        }

        public AeropuertoENT CargarAeropurtosId(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {


                con.Open();
                string sql = "select id, nombre,ciudad, pais, activo   from aeropuerto where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    AeropuertoENT aero = new AeropuertoENT
                    {


                        Id = reader.GetInt32(0),
                        Nombre = reader.GetString(1),
                        Ciudad = reader.GetString(2),
                        Pais = reader.GetString(3),
                        Activo = reader.GetBoolean(4),

                    };
                    return aero;
                }
            }
            return new AeropuertoENT();
        }

        public void Insertar(IEnumerable<AeropuertoENT> listaAeropuertos)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                


                foreach (var item in listaAeropuertos)
                {
                    
                    string sql =  "INSERT INTO public.aeropuerto( nombre, ciudad, pais )" +
                        " VALUES(@nom, @ciu, @pais)";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    
                    cmd.Parameters.AddWithValue("@nom", item.Nombre);
                    cmd.Parameters.AddWithValue("@ciu", item.Ciudad);
                    cmd.Parameters.AddWithValue("@pais", item.Pais);

                    cmd.ExecuteNonQuery();

                }

               
            }
        }


        public void Eliminar(AeropuertoENT a)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE aeropuerto " +
                    " SET activo = false " +
                    " WHERE id =@id ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", a.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public List<AeropuertoENT> CargarAeropuertos(string filtro)
        {
            List<AeropuertoENT> aeropuertos = new List<AeropuertoENT>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "select id, nombre,ciudad, pais, activo from aeropuerto " +
                    " order by nombre asc";

                if (!String.IsNullOrEmpty(filtro))
                {
                    sql = " SELECT id, nombre, ciudad, pais, activo " +
                        "FROM public.aeropuerto " +
                        "WHERE lower(nombre) like lower(@par) " +
                        "OR lower(ciudad) like lower(@par)" +
                        "OR lower(pais) like lower(@par)";
                }

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@par", filtro + "%");
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {


                    aeropuertos.Add(CargarAeropuertos(reader));
                }

            }
            return aeropuertos;
        }

        public void Delete()
        {
            try
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    string sql = "DELETE from aeropuerto ";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception)
            {

                throw;
            }
            
        }
    }
}
