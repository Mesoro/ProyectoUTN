﻿using ProyectoDAL;
using ProyectoENT;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoBOL
{
    public class AeropuertoBOL
    {
        private AeropuertoDAL adal;

        public AeropuertoBOL()
        {
            adal = new AeropuertoDAL();
        }



        internal List<AeropuertoENT> CargarAeropuertos(string filtro = "")
        {
            return adal.CargarAeropuertos(filtro);
        }

        internal void Guardar(AeropuertoENT a)
        {
            if (String.IsNullOrEmpty(a.Nombre))
            {
                throw new Exception("Datos requeridos");
            }

            else
            {
                adal.Insertar(a);
            }
        }



        internal void Eliminar(AeropuertoENT aeropuerto)
        {
            adal.Eliminar(aeropuerto);
        }

    }
}
