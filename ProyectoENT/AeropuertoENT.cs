﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoENT
{
   public  class AeropuertoENT
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Ciudad { get; set; }
        public string  Pais { get; set; }
        public bool Activo { get; set; }
    }
}
