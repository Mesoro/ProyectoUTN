﻿using ProyectDAL;
using ProyectENT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectBOL
{
    public class AerolineaBOL
    {
        private AerolineaDAL adal;

        public AerolineaBOL()
        {
            adal = new AerolineaDAL();
        }



        public List<AerolineaENT> CargarAerolineas(string filtro = "")
        {
            return adal.CargarAerolineas(filtro);
        }

        public void Guardar(IEnumerable<AerolineaENT> listaAerolineas)
        {
            if (listaAerolineas != null)
            {
                adal.Insertar(listaAerolineas);
            }


        }



        public void Eliminar(AerolineaENT aerolinea)
        {
            adal.Eliminar(aerolinea);
        }
        public void Delete()
        {
            adal.Delete();
        }
    }

}

