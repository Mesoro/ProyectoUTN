﻿using ProyectDAL;
using ProyectENT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectBOL
{
    public class AeropuertoBOL
    {
        private AeropuertoDAL adal;

        public AeropuertoBOL()
        {
            adal = new AeropuertoDAL();
        }



        public List<AeropuertoENT> CargarAeropuertos(string filtro = "")
        {
            return adal.CargarAeropuertos(filtro);
        }

        public void Guardar(IEnumerable<AeropuertoENT> listaAeropuertos)
        {
            if (listaAeropuertos!=null)
            {
                adal.Insertar(listaAeropuertos);
            }
                
            
        }



        public void Eliminar(AeropuertoENT aeropuerto)
        {
            adal.Eliminar(aeropuerto);
        }

        public void Delete()
        {
            adal.Delete();
        }
    }
}
